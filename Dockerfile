#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

FROM registry.hub.docker.com/library/alpine:edge

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

# False positive; we want to use ADD to pull the file from the internet
# hadolint ignore=DL3020
ADD "https://raw.githubusercontent.com/nlitsme/ubidump/master/ubidump.py" \
    "/usr/share/ubidump/"

# False positive; we want to use ADD to pull the file from the internet
# hadolint ignore=DL3020
ADD "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/plain/scripts/extract-ikconfig" \
    "/usr/local/bin"

# We want the latest stable version from the testing repo
# hadolint ignore=DL3013,DL3018,SC2094
RUN chmod +x "/usr/local/bin/extract-ikconfig" && \
    sed -n s/main$/testing/p "/etc/apk/repositories" >> "/etc/apk/repositories" && \
    apk add --no-cache \
        dtc \
        e2fsprogs-extra \
        ffmpeg \
        gcc \
        grep \
        lzo-dev \
        musl-dev \
        openssl \
        py3-crcmod \
        python3-dev \
        uboot-tools \
    && \
    pip3 install "python-lzo" && \
    rm -rf "/var/cache/apk/"*

COPY "./bin/*.sh" "/usr/local/bin/"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
