
// autoset
#define MSG_AUTO_TITLE           256
#define MSG_AUTO_CYCLE           257
#define MSG_AUTO_NCYCLE          258
#define MSG_AUTO_RISE            259

#define MSG_AUTO_FALL            260
#define MSG_AUTO_UNDO            261
#define MSG_AUTO_ENTER_CFG       262
#define MSG_AUTO_CFG_TITLE       263

#define MSG_AUTO_S32AUTOLOCK     264
#define MSG_AUTO_S32AUTORANGE    265
#define MSG_AUTO_S32AUTOCHAN     266
#define MSG_AUTO_S32BACK         267

#define MSG_AUTO_S32OVERLAY      268
#define MSG_AUTO_S32KEEPCOUP     269
#define MSG_AUTO_OPTION          270
#define MSG_AUTO_NOISE           271

#define INFO_LOCK_SURE           272
#define IDS_AUTO_ALL_CHAN        273
#define MSG_AUTO_PASSWORD        274
#define INF_OLD_PASSWORD         275

#define INF_NEW_PASSWORD         276
#define INF_RENEW_PASSWORD       277

// chan1
#define MSG_CHAN_ACTIVE              768
#define MSG_CHAN_ON_OFF              769
#define MSG_CHAN_COUP                770
#define MSG_CHAN_BWLIMIT             771

#define MSG_CHAN_PROBE               772
#define MSG_CHAN_UNIT                773
#define MSG_CHAN_INVERT              774
#define MSG_CHAN_FINE                775

#define MSG_CHAN_LABEL               776
#define MSG_CHAN_DLY_CAL             777
#define MSG_CHAN_LABEL_SHOW          778
#define MSG_CHAN_LABEL_NAME          779

#define MSG_CHAN_LABEL_EDIT          780
#define MSG_CHAN_SCALE               781
#define MSG_CHAN_OFFSET              782
#define MSG_CHAN_PROBE_INFO_TITLE    783

#define MSG_CHAN_PROBE_MODEL         784
#define MSG_CHAN_PROBE_DELAY         785
#define MSG_CHAN_SCALE_VALUE         786
#define MSG_CHAN_IMPEDANCE           787

#define MSG_CHAN_MORE                788
#define MSG_CHAN_VER_EXPAND          789
#define MSG_CHAN_TUNE_ZERO           790
#define MSG_CHAN_PROBE_BIAS          791

#define MSG_CHAN_PROBE_DETAIL        792
#define MSG_CHAN_PROBE_CAL           793
#define MSG_CHAN_COUPLING_TO_DC      794
#define MSG_CHAN1_CHANGED            795

#define MSG_CHAN2_CHANGED            796
#define MSG_CHAN3_CHANGED            797
#define MSG_CHAN4_CHANGED            798
#define IDS_CHAN1                    799

#define IDS_CHAN2                    800
#define IDS_CHAN3                    801
#define IDS_CHAN4                    802
#define IDS_PROBE_MFR                803

#define IDS_PROBE_MODEL              804
#define IDS_PROBE_SN                 805
#define IDS_PROBE_CAL_TIME           806

// horizontal
#define MSG_HOR_TIME_MODE                     1280
#define MSG_HOR_ZOOM_ON                       1281
#define MSG_HOR_XY_TYPE                       1282
#define MSG_HOR_XY_MODE_YT_DISP               1283

#define MSG_HOR_MENU_CHANGE                   1284
#define MSG_HOR_ACQ_MENU_CHANGE               1285
#define MSG_HOR_ACQ_SARATE                    1286
#define MSG_HOR_ACQ_AVG_TIMES                 1287

#define MSG_HOR_ACQ_MEM_DEPTH                 1288
#define MSG_HOR_ACQ_INTERLEAVE_ON             1289
#define MSG_HOR_ACQ_ANTI_ALIASING             1290
#define MSG_HOR_FINE_ON                       1291

#define MSG_HORIZONTAL_RUN                    1292
#define MSG_HOR_AUTO_ROLL                     1293
#define MSG_HORIZONTAL_TIMESCALE              1294
#define MSG_HORIZONTAL_TIMEOFFSET             1295

#define MSG_HORI_MAIN_SCALE                   1296
#define MSG_HORI_MAIN_OFFSET                  1297
#define MSG_HORI_ZOOM_SCALE                   1298
#define MSG_HORI_ZOOM_OFFSET                  1299

#define MSG_HORI_TIME_VIEW_MODE               1300
#define MSG_HORIZONTAL_XY                     1301
#define MSG_HORIZONTAL_ACQNORM_EXPAND         1302
#define MSG_HORIZONTAL_ACQNORM_EXPAND_USER    1303

#define MSG_HORIZONTAL_NORM_FIX_MORE          1304
#define MSG_HORIZONTAL_XY_MORE                1305
#define MSG_HORIZONTAL_NORM_EXPAND            1306
#define MSG_HORIZONTAL_AVG_MORE               1307

#define MSG_HOR_TITLE_ROLL                    1308
#define MSG_HOR_LA_SA_RATE                    1309
#define MSG_HOR_LA_MEM_DEPTH                  1310
#define MSG_HOR_ACQ_NULL                      1311

#define MSG_HOR_ACQ_MODE_YT                   1312
#define MSG_HOR_ACQ_MODE_ROLL                 1313
#define IDS_TO_NORMAL                         1314
#define IDS_CH1_X                             1315

#define IDS_CH2_Y                             1316
#define IDS_MEM_TO_AUTO                       1317
#define IDS_MEM_TO_1M                         1318
#define IDS_MEM_TO_25M                        1319

#define IDS_ZOOM_OFF                          1320
#define IDS_AVERAGE_OFF                       1321
#define IDS_X_Y                               1322
#define EVT_HORI_CHANGED                      1323

#define EVT_TIME_MODE_CHANGED                 1324
#define MSG_CHAN_KEY_MENU                     1325
#define MSG_CHAN_PLAY_PRE                     1326
#define MSG_CHAN_PLAY_NEXT                    1327

#define MSG_CHAN_PLAY_STOP                    1328

// trigger
#define MSG_APP_TRIGGER                     1792
#define MSG_TRIGGER_TYPE                    1793
#define MSG_TRIGGER_SOURCE_PTR              1794
#define MSG_TRIGGER_SOURCE_LA_EXT_AC        1795

#define MSG_TRIGGER_SOURCE_LA_EXT           1796
#define MSG_TRIGGER_SOURCE_LA               1797
#define MSG_TRIGGER_SOURCE                  1798
#define MSG_TRIGGER_EDGE_A                  1799

#define MSG_TRIGGER_EDGE_B                  1800
#define MSG_TRIGGER_SWEEP                   1801
#define MSG_TRIGGER_AB_A_TYPE               1802
#define MSG_TRIGGER_AB_WHEN                 1803

#define MSG_TRIGGER_EVENT_A                 1804
#define MSG_TRIGGER_EVENT_B                 1805
#define MSG_TRIGGER_EVENT_DELAY             1806
#define MSG_TRIGGER_EVENT_COUNT             1807

#define MSG_TRIGGER_SETTING_MORE_A          1808
#define MSG_TRIGGER_SETTING_MORE_B          1809
#define MSG_TRIGGER_SETTING_MORE_C          1810
#define MSG_TRIGGER_SETTING_MORE_D          1811

#define MSG_TRIGGER_DURA_MORE               1812
#define MSG_TRIGGER_DELAY_MORE_A            1813
#define MSG_TRIGGER_DELAY_MORE_B            1814
#define MSG_TRIGGER_DELAY_MORE_C            1815

#define MSG_TRIGGER_SETUP_MORE              1816
#define MSG_TRIGGER_HOLD_MORE               1817
#define MSG_TRIGGER_SETUP_HOLD_MORE         1818
#define MSG_TRIGGER_SPI_CS_MORE             1819

#define MSG_TRIGGER_COUPLING                1820
#define MSG_TRIGGER_HOLDOFF                 1821
#define MSG_TRIGGER_NOISE                   1822
#define MSG_TRIGGER_POLARITY                1823

#define MSG_TRIGGER_LIMIT_UPPER             1824
#define MSG_TRIGGER_LIMIT_LOWER             1825
#define MSG_TRIGGER_SLOPE_POLARITY          1826
#define MSG_TRIGGER_PULSE_WHEN              1827

#define MSG_TRIGGER_SLOPE_WHEN              1828
#define MSG_TRIGGER_LEVELSELECT             1829
#define MSG_TRIGGER_VIDEO_SYNC              1830
#define MSG_TRIGGER_VIDEO_STANDARD          1831

#define MSG_TRIGGER_VIDEO_LINENUM           1832
#define MSG_TRIGGER_CODE_ALL                1833
#define MSG_TRIGGER_SET_CODE                1834
#define MSG_TRIGGER_CODE                    1835

#define MSG_TRIGGER_SET_DATA                1836
#define MSG_TRIGGER_DURATION_WHEN           1837
#define MSG_TRIGGER_TIMEOUT_TIME            1838
#define MSG_TRIGGER_RUNT_WHEN               1839

#define MSG_TRIGGER_SETTING_MORE            1840
#define MSG_TRIGGER_WINDOW_POS              1841
#define MSG_TRIGGER_WINDOW_TIME             1842
#define MSG_TRIGGER_OVER_POS                1843

#define MSG_TRIGGER_OVER_TIME               1844
#define MSG_TRIGGER_OVER_SLOPE              1845
#define MSG_TRIGGER_DELAY_WHEN              1846
#define MSG_TRIGGER_DELAY_SRCA              1847

#define MSG_TRIGGER_EDGEA                   1848
#define MSG_TRIGGER_DELAY_SRCB              1849
#define MSG_TRIGGER_EDGEB                   1850
#define MSG_TRIGGER_DELAY_TIME              1851

#define MSG_TRIGGER_SETUP_SDA               1852
#define MSG_TRIGGER_SETUP_SCL               1853
#define MSG_TRIGGER_SETUP_DATA              1854
#define MSG_TRIGGER_SETUP_WHEN              1855

#define MSG_TRIGGER_SETUP_TIME              1856
#define MSG_TRIGGER_HOLD_TIME               1857
#define MSG_TRIGGER_NTH_IDLETIME            1858
#define MSG_TRIGGER_NTH_EDGE                1859

#define MSG_TRIGGER_RS232_MORE              1860
#define MSG_TRIGGER_RS232_WHEN              1861
#define MSG_TRIGGER_RS232_POLARITY          1862
#define MSG_TRIGGER_RS232_STOPBIT           1863

#define MSG_TRIGGER_RS232_CHECK             1864
#define MSG_TRIGGER_RS232_BAUDRATE          1865
#define MSG_TRIGGER_RS232_BAUDRATE_SUB_1    1866
#define MSG_TRIGGER_RS232_DATAWIDTH         1867

#define MSG_TRIGGER_RS232_DATA              1868
#define MSG_TRIGGER_I2C_SCL                 1869
#define MSG_TRIGGER_I2C_SDA                 1870
#define MSG_TRIGGER_I2C_WHEN                1871

#define MSG_TRIGGER_I2C_ADDRTYPE            1872
#define MSG_TRIGGER_I2C_ADDRDATA_BITS       1873
#define MSG_TRIGGER_I2C_ADDRESS             1874
#define MSG_TRIGGER_I2C_BYTELENGTH          1875

#define MSG_TRIGGER_I2C_CURRBIT             1876
#define MSG_TRIGGER_I2C_DATA                1877
#define MSG_TRIGGER_I2C_ALLBITS             1878
#define MSG_TRIGGER_I2C_DIRECTION           1879

#define MSG_TRIGGER_I2C_ADDR_SETUP          1880
#define MSG_TRIGGER_APP_I2C_ADDR_SETUP      1881
#define MSG_TRIGGER_DATA_SETUP              1882
#define MSG_TRIGGER_ADDRDATA_SETUP          1883

#define MSG_TRIGGER_SPI_SCL                 1884
#define MSG_TRIGGER_SPI_SDA                 1885
#define MSG_TRIGGER_SPI_CS                  1886
#define MSG_TRIGGER_SPI_TIMEOUT             1887

#define MSG_TRIGGER_SPI_WHEN                1888
#define MSG_TRIGGER_SPI_CSMODE              1889
#define MSG_TRIGGER_SPI_DATA_MENU           1890
#define MSG_TRIGGER_SPI_DATABITS            1891

#define MSG_TRIGGER_SPI_CURRBIT             1892
#define MSG_TRIGGER_SPI_DATA                1893
#define MSG_TRIGGER_SPI_ALLBITS             1894
#define MSG_TRIGGER_SPI_MORE                1895

#define MSG_TRIGGER_CAN_WHEN                1896
#define MSG_TRIGGER_CAN_MORE                1897
#define MSG_TRIGGER_CAN_ID_EXTENDED         1898
#define MSG_TRIGGER_CURR_BIT                1899

#define MSG_TRIGGER_CAN_DATA_BYTE           1900
#define MSG_TRIGGER_CAN_DEFINE              1901
#define MSG_TRIGGER_CAN_ID_FILTER           1902
#define MSG_TRIGGER_CAN_SINGNAL             1903

#define MSG_TRIGGER_CAN_BAUD                1904
#define MSG_TRIGGER_CAN_BAUD_SUB_1          1905
#define MSG_TRIGGER_CAN_SAMPLE_POINT        1906
#define MSG_TRIGGER_CAN_STANDARD            1907

#define MSG_APP_TRIGGER_CAN_MORE            1908
#define MSG_TRIGGER_LIN_WHEN                1909
#define MSG_TRIGGER_LIN_ERR_TYPE            1910
#define MSG_TRIGGER_LIN_MORE                1911

#define MSG_TRIGGER_LIN_ID                  1912
#define MSG_TRIGGER_LIN_DATA_BIT            1913
#define MSG_TRIGGER_LIN_DATA_BYTE           1914
#define MSG_TRIGGER_LIN_VERSION             1915

#define MSG_TRIGGER_LIN_BAUD                1916
#define MSG_TRIGGER_LIN_BAUD_SUB_1          1917
#define MSG_TRIGGER_LIN_SAMPLE_POINT        1918
#define MSG_APP_TRIGGER_LIN_MORE            1919

#define MSG_TRIGGER_IIS_SCLK                1920
#define MSG_TRIGGER_IIS_SLOPE               1921
#define MSG_TRIGGER_IIS_WS                  1922
#define MSG_TRIGGER_IIS_WS_LOW              1923

#define MSG_TRIGGER_IIS_SDA                 1924
#define MSG_TRIGGER_IIS_WHEN                1925
#define MSG_TRIGGER_IIS_DATA                1926
#define MSG_TRIGGER_IIS_DATA_MIN            1927

#define MSG_TRIGGER_IIS_DATA_MAX            1928
#define MSG_TRIGGER_IIS_CURR_BIT            1929
#define MSG_TRIGGER_IIS_WIDTH               1930
#define MSG_TRIGGER_IIS_USER_WIDTH          1931

#define MSG_TRIGGER_IIS_ALIGNMENT           1932
#define MSG_TRIGGER_IIS_MORE                1933
#define MSG_APP_TRIGGER_IIS_MORE            1934
#define MSG_TRIGGER_FLEXRAY_WHEN            1935

#define MSG_TRIGGER_FLEXRAY_POST_TYPE       1936
#define MSG_TRIGGER_FLEXRAY_FRAME_TYPE      1937
#define MSG_TRIGGER_FLEXRAY_SYMBOL_TYPE     1938
#define MSG_TRIGGER_FLEXRAY_ERROR_TYPE      1939

#define MSG_TRIGGER_FLEXRAY_ID_COMP         1940
#define MSG_TRIGGER_FLEXRAY_CYC_COMP        1941
#define MSG_TRIGGER_FLEXRAY_BAUD            1942
#define MSG_TRIGGER_FLEXRAY_CH_A_B          1943

#define MSG_TRIGGER_FLEXRAY_ID_MIN          1944
#define MSG_TRIGGER_FLEXRAY_ID_MAX          1945
#define MSG_TRIGGER_FLEXRAY_CYC_MIN         1946
#define MSG_TRIGGER_FLEXRAY_CYC_MAX         1947

#define MSG_TRIGGER_FLEXRAY_DEFINE          1948
#define MSG_TRIGGER_FLEXRAY_MORE            1949
#define MSG_APP_TRIGGER_FLEXRAY_MORE        1950
#define MSG_TRIGGER_1553B_WHEN              1951

#define MSG_TRIGGER_1553B_SYNC_TYPE         1952
#define MSG_TRIGGER_1553B_DATA_COMP         1953
#define MSG_TRIGGER_1553B_DATA_MIN          1954
#define MSG_TRIGGER_1553B_DATA_MAX          1955

#define MSG_TRIGGER_1553B_TIME              1956
#define MSG_TRIGGER_1553B_RTA               1957
#define MSG_TRIGGER_1553B_RTA_11            1958
#define MSG_TRIGGER_1553B_ERR_TYPE          1959

#define MSG_TRIGGER_1553B_MORE_A            1960
#define MSG_TRIGGER_1553B_MORE              1961
#define MSG_APP_TRIGGER_1553B_MORE_A        1962
#define MSG_APP_TRIGGER_1553B_MORE          1963

#define MSG_TRIGGER_SPACE                   1964
#define MSG_TRIGGER_LEVEL                   1965
#define MSG_TRIGGER_LEVEL_Z                 1966
#define MSG_TRIGGER_SET_LEVEL               1967

#define MSG_TRIGGER_DISP_LEVEL              1968
#define MSG_TRIGGER_APPLY_LEVEL             1969
#define MSG_TRIGGER_SINGLE                  1970
#define MSG_TRIGGER_FORCE                   1971

#define CMD_SCPI_TRIGGER_LEVEL              1972
#define CMD_SCPI_TRIGGER_LEVEL_H            1973
#define CMD_SCPI_TRIGGER_LEVEL_L            1974
#define CMD_SCPI_TRIGGER_LEVEL_CLK          1975

#define CMD_SCPI_TRIGGER_LEVEL_DATA         1976
#define CMD_SCPI_TRIGGER_LEVEL_CS           1977
#define CMD_SCPI_TRIGGER_PULSE_WIDTH        1978
#define CMD_SCPI_TRIGGER_SLOPE_TIME         1979

#define CMD_TRIGGER_SET_MSG_ENAB            1980
#define CMD_TRIGGER_ALL_SPY_LICENSE         1981
#define CMD_TRIGGER_MODE_KEY                1982
#define CMD_TRIGGER_SYST_STOP               1983

#define CMD_TRIGGER_ACTIVE                  1984
#define CMD_TRIGGER_EDGE_CFG                1985
#define CMD_TRIGGER_PULSE_CFG               1986
#define CMD_TRIGGER_SLOPE_CFG               1987

#define CMD_TRIGGER_VIDEO_CFG               1988
#define CMD_TRIGGER_PATTERN_CFG             1989
#define CMD_TRIGGER_DURATION_CFG            1990
#define CMD_TRIGGER_RUNT_CFG                1991

#define CMD_TRIGGER_OVER_CFG                1992
#define CMD_TRIGGER_WINDOW_CFG              1993
#define CMD_TRIGGER_TIMEOUT_CFG             1994
#define CMD_TRIGGER_DELAY_CFG               1995

#define CMD_TRIGGER_SETUP_HODE_CFG          1996
#define CMD_TRIGGER_NTH_CFG                 1997
#define CMD_TRIGGER_RS232_CFG               1998
#define CMD_TRIGGER_I2S_CFG                 1999

#define CMD_TRIGGER_SPI_CFG                 2000
#define CMD_TRIGGER_AB_CFG                  2001
#define CMD_TRIGGER_CAN_CFG                 2002
#define CMD_TRIGGER_LIN_CFG                 2003

#define CMD_TRIGGER_FLEXRAY_CFG             2004
#define CMD_TRIGGER_IIS_CFG                 2005
#define CMD_TRIGGER_USB_CFG                 2006
#define CMD_TRIGGER_1553B_CFG               2007

#define CMD_TRIGGER_1EIRE_CFG               2008
#define STRING_TRIGGER_SETTING              2009
#define STRING_TRIGGER_LEVEL                2010
#define STRING_TRIGGER_LEVEL_A              2011

#define STRING_TRIGGER_LEVEL_B              2012
#define STRING_TRIGGER_INVERT_HINT          2013
#define STRING_TRIGGER_AUTO                 2014
#define STRING_TRIGGER_NORMAL               2015

#define STRING_TRIGGER_SINGLE               2016
#define CMD_SCPI_TRIGGER_DATA_IIS           2017
#define CMD_SCPI_TRIGGER_DATA_1553          2018
#define CMD_SCPI_PATTERN_GET_CODE           2019

#define CMD_SCPI_DURATION_GET_CODE          2020

// selfcal
#define MSG_SELF_CAL             2304
#define MSG_SELF_CAL_START       2305
#define MSG_SELF_CAL_QUIT        2306
#define MSG_SELF_CAL_VERTICAL    2307

#define MSG_SELF_CAL_ADC         2308
#define MSG_SELF_CAL_DELAY       2309
#define MSG_CAL_LOAD_USER        2310
#define MSG_CAL_LOAD_DEFAULT     2311

#define MSG_CAL_EXPORT_A         2312
#define MSG_CAL_SPACE            2313
#define MSG_CAL_ITEMS            2314
#define MSG_SELF_CAL_WINDOW      2315


// measure
#define MSG_APP_MEASURE                    2816
#define MSG_APP_MEAS_ADD_MENU              2817
#define MSG_APP_MEAS_REMOVE_MENU           2818
#define MSG_APP_MEAS_STAT_MENU             2819

#define MSG_APP_MEAS_SRCA                  2820
#define MSG_APP_MEAS_SRCB                  2821
#define MSG_APP_MEAS_CAT                   2822
#define MSG_APP_MEAS_ANALYZE_MENU          2823

#define MSG_APP_MEAS_COUNTER_MENU          2824
#define MSG_APP_MEAS_DVM_MENU              2825
#define MSG_APP_MEAS_UPA_MENU              2826
#define MSG_APP_MEAS_HISTO_MENU            2827

#define MSG_APP_EYE_MENU                   2828
#define MSG_APP_JITTER_MENU                2829
#define MSG_APP_MEAS_ZONE_MENU             2830
#define MSG_APP_MEAS_ALL_SRC               2831

#define MSG_APP_MEAS_SET_MENU              2832
#define MSG_APP_MEAS_SET_TYPE              2833
#define MSG_APP_MEAS_TH_SRC                2834
#define MSG_APP_MEAS_TH_HIGH_TYPE          2835

#define MSG_APP_MEAS_TH_HIGH_TYPE_SUB_1    2836
#define MSG_APP_MEAS_TH_MID_TYPE           2837
#define MSG_APP_MEAS_TH_MID_TYPE_SUB_1     2838
#define MSG_APP_MEAS_TH_LOW_TYPE           2839

#define MSG_APP_MEAS_TH_LOW_TYPE_SUB_1     2840
#define MSG_APP_MEAS_TH_DEFAULT            2841
#define MSG_APP_MEAS_SET_STATE_METHOD      2842
#define MSG_APP_MEAS_SET_TOP_METHOD        2843

#define MSG_APP_MEAS_SET_BASE_METHOD       2844
#define MSG_APP_MEAS_RANGE_MODE            2845
#define MSG_APP_MEAS_REGION                2846
#define MSG_APP_MEAS_RANGE_CURSOR_AX       2847

#define MSG_APP_MEAS_RANGE_CURSOR_BX       2848
#define MSG_APP_MEAS_RANGE_CURSOR_ABX      2849
#define MSG_APP_MEAS_CLEAR_ONE             2850
#define MSG_APP_MEAS_CLEAR_ALL             2851

#define MSG_APP_MEAS_INDICATOR             2852
#define MSG_APP_MEAS_STAT_ENABLE           2853
#define MSG_APP_MEAS_TREND_ENABLE          2854
#define MSG_APP_MEAS_STAT_RESET            2855

#define MSG_APP_MEAS_STAT_COUNT            2856
#define MSG_APP_MEAS_TO_REMOVE             2857
#define MSG_APP_MEAS_SPACE                 2858
#define MSG_APP_MEAS_TEST                  2859


// counter
#define MSG_COUNTER                 3328
#define MSG_COUNTER_1_ENABLE        3329
#define MSG_COUNTER_1_SRC           3330
#define MSG_COUNTER_1_MEAS_TYPE     3331

#define MSG_COUNTER_1_RESOLUTION    3332
#define MSG_COUNTER_1_STAT          3333
#define MSG_COUNTER_1_TOT_CLEAR     3334

// dvm
#define MSG_DVM                3840
#define MSG_DVM_ENABLE         3841
#define MSG_DVM_SRC            3842
#define MSG_DVM_MODE           3843

#define MSG_DVM_LIMITS_MENU    3844
#define MSG_DVM_BEEP_ENABLE    3845
#define MSG_DVM_LIMIT_TYPE     3846
#define MSG_DVM_LIMIT_UPPER    3847

#define MSG_DVM_LIMIT_LOWER    3848

// upa
#define MSG_APP_UPA               4352
#define MSG_UPA_TYPE              4353
#define MSG_UPA_POWER_SOURCE      4354
#define MSG_UPA_POWER_VOLT        4355

#define MSG_UPA_POWER_CURR        4356
#define MSG_UPA_REF_LEVEL         4357
#define MSG_UPA_REFL_TYPE         4358
#define MSG_UPA_REFL_PCT_HIGH     4359

#define MSG_UPA_REFL_PCT_MID      4360
#define MSG_UPA_REFL_PCT_LOW      4361
#define MSG_UPA_REFL_ABS_HIGH     4362
#define MSG_UPA_REFL_ABS_MID      4363

#define MSG_UPA_REFL_ABS_LOW      4364
#define MSG_UPA_REFL_DEFAULT      4365
#define MSG_UPA_POWER_AUTOSET     4366
#define MSG_UPA_STAT_COUNT        4367

#define MSG_UPA_STAT_RESET        4368
#define MSG_UPA_POWER_DISP        4369
#define MSG_UPA_POWER_FREF        4370
#define MSG_UPA_POWER_CYC         4371

#define MSG_UPA_AUTODESKEW        4372
#define MSG_UPA_POWER_TIPS        4373
#define MSG_UPA_RIPPLE_SOURCE     4374
#define MSG_UPA_RIPPLE_AUTOSET    4375

#define MSG_UPA_RIPPLE_DISP       4376
#define MSG_UPA_RIPPLE_TIPS       4377
#define MSG_APP_RIPPLE_TITLE      4378
#define MSG_APP_RIPPLE_CURR       4379

#define MSG_APP_RIPPLE_AVG        4380
#define MSG_APP_RIPPLE_MIN        4381
#define MSG_APP_RIPPLE_MAX        4382
#define MSG_APP_RIPPLE_DEV        4383

#define MSG_APP_RIPPLE_CNT        4384
#define IDS_RIPPLE                4385
#define IDS_POWERQ                4386

// eyejit
#define MSG_EYEJIT                  4864
#define MSG_JITTER                  4865
#define MSG_EYE_SRC                 4866
#define MSG_JIT_SRC                 4867

#define MSG_EYEJIT_CR_SRC           4868
#define MSG_EYEJIT_SRC              4869
#define MSG_EYEJIT_SETTINGS         4870
#define MSG_EYEJIT_YOFFSET          4871

#define MSG_EYEJIT_CLOCK            4872
#define MSG_EYEJIT_PLLCR            4873
#define MSG_EYEJIT_EYEMEAS          4874
#define MSG_EYEJIT_JITMEAS          4875

#define MSG_EYEJIT_MORE             4876
#define MSG_EYEJIT_EYEMASK          4877
#define MSG_EYEJIT_EYECOLOR         4878
#define MSG_EYEJIT_SRC_SELSTD       4879

#define MSG_EYEJIT_SRC_SELTYPE      4880
#define MSG_EYEJIT_THRE_TYPE        4881
#define MSG_EYEJIT_HIGHTHRE_PER     4882
#define MSG_EYEJIT_MIDTHRE_PER      4883

#define MSG_EYEJIT_LOWTHRE_PER      4884
#define MSG_EYEJIT_HIGHTHRE_VAL     4885
#define MSG_EYEJIT_MIDTHRE_VAL      4886
#define MSG_EYEJIT_LOWTHRE_VAL      4887

#define MSG_EYEJIT_EN               4888
#define MSG_EYEJIT_EMEAS_NUM        4889
#define MSG_EYEJIT_MEAS_EN          4890
#define MSG_EYEJIT_JIT_TYPE         4891

#define MSG_EYEJIT_JIT_TRACK        4892
#define MSG_EYEJIT_JIT_SPEC         4893
#define MSG_EYEJIT_JIT_HISTO        4894
#define MSG_EYEJIT_MASK_EN          4895

#define MSG_EYEJIT_MASK_SELTYPE     4896
#define MSG_EYEJIT_COLOR_COUNT      4897
#define MSG_EYEJIT_RESET            4898
#define MSG_JITTER_RESET            4899

#define MSG_EYEJIT_CR_SELMETHOD     4900
#define MSG_EYEJIT_CR_DATEFREQ      4901
#define MSG_EYEJIT_CR_FREQTYPE      4902
#define MSG_EYEJIT_CR_PLLORDER      4903

#define MSG_EYEJIT_CR_PLLWIDTH      4904
#define MSG_EYEJIT_CR_DAMPFACTOR    4905
#define MSG_EYEJIT_PARALLELCR       4906
#define MSG_EYEJIT_JIT_EDGE         4907

#define MSG_JITTER_MEASURE          4908
#define MSG_JITTER_EN               4909
#define MSG_EYEJIT_JIT_AUTO         4910
#define MSG_EYE_OVERLAP             4911


// cursor
#define MSG_CURSOR                    5376
#define MSG_CURSOR_S32CURSORMODE      5377
#define MSG_CURSOR_S32MTYPE           5378
#define MSG_CURSOR_S23MAREA           5379

#define MSG_CURSOR_S32MANUALSRC       5380
#define MSG_CURSOR_S32MTIMEUNIT       5381
#define MSG_CURSOR_S32MHAPOS          5382
#define MSG_CURSOR_S32MHBPOS          5383

#define MSG_CURSOR_S32MHABPOS         5384
#define MSG_CURSOR_PMHRANGE           5385
#define MSG_CURSOR_S32MVUNIT          5386
#define MSG_CURSOR_S32MVAPOS          5387

#define MSG_CURSOR_S32MVBPOS          5388
#define MSG_CURSOR_S32MVABPOS         5389
#define MSG_CURSOR_PMVRANGE           5390
#define MSG_CURSOR_LATYPE             5391

#define MSG_CURSOR_S32TASRC           5392
#define MSG_CURSOR_S32TBSRC           5393
#define MSG_CURSOR_S32TAPOS           5394
#define MSG_CURSOR_S32TBPOS           5395

#define MSG_CURSOR_S32TABPOS          5396
#define MSG_CURSOR_S32AUTOITEM        5397
#define MSG_CURSOR_S32XY_VALS         5398
#define MSG_CURSOR_S32XY_XA           5399

#define MSG_CURSOR_S32XY_XB           5400
#define MSG_CURSOR_S32XY_YA           5401
#define MSG_CURSOR_S32XY_YB           5402
#define MSG_CURSOR_S32XY_XAB          5403

#define MSG_CURSOR_S32XY_YAB          5404
#define MSG_CURSOR_LISAJOUS           5405
#define MSG_CURSOR_XY_MODE            5406
#define MSG_CURSOR_S32XY_TRACK_AX     5407

#define MSG_CURSOR_S32XY_TRACK_BX     5408
#define MSG_CURSOR_S32XY_TRACK_ABX    5409
#define MSG_CURSOR_TRACK_MODE         5410
#define MSG_CURSOR_S32TAPOS_V         5411

#define MSG_CURSOR_S32TBPOS_V         5412
#define MSG_CURSOR_S32TABPOS_V        5413
#define MSG_CURSOR_INDICATOR_ONOFF    5414
#define MSG_CURSOR_S32MHMORE          5415

#define MSG_CURSOR_S32MVMORE          5416
#define MSG_XY_MANUAL_MORE            5417

// mask
#define MSG_APP_MASK            5888
#define MSG_MASK_ENABLE         5889
#define MSG_MASK_SOURCE         5890
#define MSG_MASK_OPERATE        5891

#define MSG_MASK_SELECT         5892
#define MSG_MASK_CREATE_MENU    5893
#define MSG_MASK_RANGE          5894
#define MSG_MASK_X_MASK         5895

#define MSG_MASK_Y_MASK         5896
#define MSG_MASK_CREATE         5897
#define MSG_MASK_RANGE_A        5898
#define MSG_MASK_RANGE_B        5899

#define MSG_MASK_RANGE_AB       5900
#define MSG_MASK_ERR_ACTION     5901
#define MSG_MASK_SHOW_STAT      5902
#define MSG_MASK_RESET_STAT     5903

#define MSG_MASK_EXPORT         5904
#define MSG_MASK_LOAD           5905
#define MSG_MASK_SAVE           5906
#define MSG_MASK_OUT_EVENT      5907

#define MSG_MASK_OUT_ONOFF      5908
#define MSG_MASK_OUT_PULSE      5909
#define MSG_MASK_OUT_HL         5910
#define MSG_MASK_OPTION         5911

#define IDS_MASK_MATCH          5912
#define IDS_MASK_MISCC          5913
#define IDS_MASK_TOTAL          5914
#define IDS_MASK_SIGMA          5915


// math
#define MSG_MATH                        6400
#define MSG_MATH_EN                     6401
#define MSG_MATH_S32MATHOPERATOR        6402
#define MSG_MATH_S32MATHINVERT          6403

#define MSG_MATH_S32ARITHA              6404
#define MSG_MATH_S32ARITHB              6405
#define MSG_MATH_S32FFTSRC              6406
#define MSG_MATH_FFT_H_CENTER           6407

#define MSG_MATH_FFT_H_SPAN             6408
#define MSG_MATH_FFT_H_START            6409
#define MSG_MATH_FFT_H_END              6410
#define MSG_MATH_FFT_SCALE              6411

#define MSG_MATH_S32FFTWINDOW           6412
#define MSG_MATH_S32FFTSCR              6413
#define MSG_MATH_S32FFTUNIT             6414
#define MSG_MATH_FFT_X_TYPE             6415

#define MSG_MATH_FFT_DC_SUPPRESS        6416
#define MSG_MATH_FFT_PEAK_TITLE         6417
#define MSG_MATH_FFT_PEAK_ENABLE        6418
#define MSG_MATH_FFT_PEAK_MAXPEAKS      6419

#define MSG_MATH_FFT_PEAK_THRESHOLD     6420
#define MSG_MATH_FFT_PEAK_EXCURSION     6421
#define MSG_MATH_FFT_PEAK_TABELORDER    6422
#define MSG_MATH_FFT_PEAK_EXPORT        6423

#define MSG_MATH_S32LOGICA              6424
#define MSG_MATH_S32LOGICB              6425
#define MSG_MATH_S32LOGIC1THRE          6426
#define MSG_MATH_S32LOGIC2THRE          6427

#define MSG_MATH_S32LOGIC3THRE          6428
#define MSG_MATH_S32LOGIC4THRE          6429
#define MSG_MATH_S32LOGICSENS           6430
#define MSG_MATH_S32FUNCSCALE           6431

#define MSG_MATH_S32DIFFSENS            6432
#define MSG_MATH_OPT_TITLE              6433
#define MSG_MATH_ENTER_VAR_OPT          6434
#define MSG_MATH_ENTER_LA_OPT           6435

#define MSG_MATH_ENTER_INTG_OPT         6436
#define MSG_MATH_ENTER_DIFF_OPT         6437
#define MSG_MATH_ENTER_LINE_OPT         6438
#define MSG_MATH_ENTER_FFT_OPT          6439

#define MSG_MATH_S32RSTVSCALE           6440
#define MSG_MATH_S32LABELCFG            6441
#define MSG_MATH_S32SHOWLABEL           6442
#define MSG_MATH_S32PRESETINDEX         6443

#define MSG_MATH_S32LABELEDIOR          6444
#define MSG_MATH_MATH_INTGOPT_BIAS      6445
#define MSG_MATH_MATH_LINEOPT_A         6446
#define MSG_MATH_MATH_LINEOPT_B         6447

#define MSG_MATH_LOGIC_SCALE            6448
#define MSG_MATH_VIEW_OFFSET            6449
#define MSG_MATH_FFT_OFFSET             6450
#define MSG_MATH_LOGIC_OFFSET           6451

#define MSG_MATH_EXPAND                 6452
#define MSG_MATH_COLOR_ONOFF            6453
#define MSG_MATH_COLOR_RESET            6454
#define MSG_MATH_S32FILTER_LP           6455

#define MSG_MATH_S32FILTER_HP           6456
#define MSG_MATH_S32FILTER_BP1          6457
#define MSG_MATH_S32FILTER_BP2          6458
#define MSG_MATH_S32FILTER_BT1          6459

#define MSG_MATH_S32FILTER_BT2          6460
#define MSG_MATH_ARITH2_MORE            6461
#define MSG_MATH_LOGIC2_MORE            6462
#define MSG_MATH_LOGIC1_MORE            6463

#define MSG_MATH_FUNC_MORE              6464
#define MSG_MATH_INTG_MORE              6465
#define MSG_MATH_DIFF_MORE              6466
#define MSG_MATH_LINE_MORE              6467

#define MSG_MATH_FILTER_LP_MORE         6468
#define MSG_MATH_FILTER_HP_MORE         6469
#define MSG_MATH_FILTER_BP_MORE         6470
#define MSG_MATH_FILTER_BT_MORE         6471

#define MSG_MATH_FFT_MORE               6472
#define MSG_MATH_FFT_OPT_MORE           6473
#define IDS_MATH1                       6474
#define IDS_MATH2                       6475

#define IDS_MATH3                       6476
#define IDS_MATH4                       6477

// mathsel
#define MSG_APP_MATHSEL    6912
#define MSG_APP_MATH1      6913
#define MSG_APP_MATH2      6914
#define MSG_APP_MATH3      6915

#define MSG_APP_MATH4      6916

// mathfftsel
#define MSG_MATHFFTSEL_FFTSEL    7424
#define MSG_MATHFFTSEL_FFT1      7425
#define MSG_MATHFFTSEL_FFT2      7426
#define MSG_MATHFFTSEL_FFT3      7427

#define MSG_MATHFFTSEL_FFT4      7428

// ref
#define MSG_APP_REF            7936
#define MSG_REF_SOURCE         7937
#define MSG_REF_CHANNEL        7938
#define MSG_REF_SAVE           7939

#define MSG_REF_TIMESCALE      7940
#define MSG_REF_OFFSET         7941
#define MSG_REF_OPTION         7942
#define MSG_REF_LABEL_ONOFF    7943

#define MSG_REF_LABEL_NAME     7944
#define MSG_REF_COLOR          7945
#define MSG_REF_DETAILS        7946
#define MSG_REF_DISPTYPE       7947

#define MSG_REF_ONOFF          7948
#define MSG_REF_IMPORT         7949
#define MSG_REF_EXPORT         7950
#define MSG_REF_LABEL_EDIT     7951

#define MSG_REF_LABEL          7952
#define MSG_REF_HIDDEN         7953
#define MSG_REF_RESET          7954
#define MSG_REF_VOLT           7955

#define MSG_REF_POS            7956
#define MSG_WAVE_POS_HELP      7957
#define MSG_WAVE_SCALE_HELP    7958

// vdecode
#define MSG_APP_DECODE                 8448
#define MSG_DECODE_TYPE                8449
#define MSG_DECODE_ONOFF               8450
#define MSG_DECODE_PAL_CLK_MENU        8451

#define MSG_DECODE_PAL_DAT_MENU        8452
#define MSG_DECODE_PAL_CLK             8453
#define MSG_DECODE_PAL_CLK_EDGE        8454
#define MSG_DECODE_PAL_CLK_DLY         8455

#define MSG_DECODE_PAL_CLK_THRE        8456
#define MSG_DECODE_PAL_DAT_THRE        8457
#define MSG_DECODE_PAL_BUS             8458
#define MSG_DECODE_BUS_WIDTH           8459

#define MSG_DECODE_BUS_BITX            8460
#define MSG_DECODE_BUS_CH              8461
#define MSG_DECODE_PAL_NRJ             8462
#define MSG_DECODE_PAL_ENDIAN          8463

#define MSG_DECODE_PAL_SET             8464
#define MSG_DECODE_NRJ_TIME            8465
#define MSG_DECODE_BUS_GRAPH           8466
#define MSG_DECODE_POLARITY            8467

#define MSG_DECODE_SPACE               8468
#define MSG_DECODE_SETTINGS            8469
#define MSG_DECODE_DISPLAY             8470
#define MSG_DECODE_FORMAT              8471

#define MSG_DECODE_POS                 8472
#define MSG_DECODE_LABEL               8473
#define MSG_DECODE_EVT_MENU            8474
#define MSG_DECODE_EVT                 8475

#define MSG_DECODE_EVT_EXPORT          8476
#define MSG_DECODE_EVT_JUMP            8477
#define MSG_DECODE_EVT_FORMAT          8478
#define MSG_DECODE_EVT_BUSX            8479

#define MSG_DECODE_EVT_VIEW            8480
#define MSG_DECODE_RS232_BAUD          8481
#define MSG_DECODE_RS232_BAUD_SUB_1    8482
#define MSG_DECODE_SIG_MENU            8483

#define MSG_DECODE_RS232_SET           8484
#define MSG_DECODE_COPY_TRIG           8485
#define MSG_DECODE_RS232_TX            8486
#define MSG_DECODE_RS232_RX            8487

#define MSG_DECODE_TX_THRE             8488
#define MSG_DECODE_RX_THRE             8489
#define MSG_DECODE_RS232_POL           8490
#define MSG_DECODE_RS232_WIDTH         8491

#define MSG_DECODE_RS232_STOP          8492
#define MSG_DECODE_RS232_PARITY        8493
#define MSG_DECODE_RS232_PACKEN        8494
#define MSG_DECODE_RS232_ENDIAN        8495

#define MSG_DECODE_RS232_PACKEND       8496
#define MSG_DECODE_I2C_SIG             8497
#define MSG_DECODE_I2C_SCL             8498
#define MSG_DECODE_I2C_SDA             8499

#define MSG_DECODE_SCL_THRE            8500
#define MSG_DECODE_SDA_THRE            8501
#define MSG_DECODE_I2C_EXC             8502
#define MSG_DECODE_I2C_RW              8503

#define MSG_DECODE_MODE_MENU           8504
#define MSG_DECODE_SPI_SIG             8505
#define MSG_DECODE_SPI_SET             8506
#define MSG_DECODE_SPI_MODE            8507

#define MSG_DECODE_SPI_CS              8508
#define MSG_DECODE_SPI_CLK             8509
#define MSG_DECODE_SPI_MISO            8510
#define MSG_DECODE_SPI_MOSI            8511

#define MSG_DECODE_CS_THRE             8512
#define MSG_DECODE_CLK_THRE            8513
#define MSG_DECODE_MISO_THRE           8514
#define MSG_DECODE_MOSI_THRE           8515

#define MSG_DECODE_CLK_EDGE            8516
#define MSG_DECODE_DAT_POL             8517
#define MSG_DECODE_CS_POL              8518
#define MSG_DECODE_SPI_TMO             8519

#define MSG_DECODE_SPI_WIDTH           8520
#define MSG_DECODE_SPI_ENDIAN          8521
#define MSG_DECODE_LIN_SRC             8522
#define MSG_DECODE_LIN_THRE            8523

#define MSG_DECODE_LIN_SET             8524
#define MSG_DECODE_LIN_BAUD            8525
#define MSG_DECODE_LIN_BAUD_SUB_1      8526
#define MSG_DECODE_LIN_POL             8527

#define MSG_DECODE_LIN_VER             8528
#define MSG_DECODE_LIN_PARITY          8529
#define MSG_DECODE_CAN_SIGNAL          8530
#define MSG_DECODE_CAN_SET             8531

#define MSG_DECODE_CAN_SRC             8532
#define MSG_DECODE_CAN_THRE            8533
#define MSG_DECODE_CAN_BAUD            8534
#define MSG_DECODE_CAN_BAUD_SUB_1      8535

#define MSG_DECODE_CANFD_BAUD          8536
#define MSG_DECODE_CANFD_BAUD_SUB_1    8537
#define MSG_DECODE_CAN_SAMP            8538
#define MSG_DECODE_CANFD_SAMP          8539

#define MSG_DECODE_FLEX_SRC            8540
#define MSG_DECODE_FLEX_THRE           8541
#define MSG_DECODE_FLEX_BAUD           8542
#define MSG_DECODE_FLEX_SET            8543

#define MSG_DECODE_FLEX_SAMP           8544
#define MSG_DECODE_FLEX_SIGNAL         8545
#define MSG_DECODE_FLEX_CHANNEL        8546
#define MSG_DECODE_I2S_SIGNAL          8547

#define MSG_DECODE_I2S_BUS             8548
#define MSG_DECODE_I2S_SCLK            8549
#define MSG_DECODE_I2S_WS              8550
#define MSG_DECODE_I2S_DATA            8551

#define MSG_DECODE_I2S_SCLK_THRE       8552
#define MSG_DECODE_I2S_DATA_THRE       8553
#define MSG_DECODE_I2S_WS_THRE         8554
#define MSG_DECODE_I2S_SCLKEDGE        8555

#define MSG_DECODE_I2S_WORD            8556
#define MSG_DECODE_I2S_RECEIVE         8557
#define MSG_DECODE_I2S_ALIGN           8558
#define MSG_DECODE_I2S_WSLOW           8559

#define MSG_DECODE_I2S_ENDIAN          8560
#define MSG_DECODE_I2S_POL             8561
#define MSG_DECODE_1553B_SIGNAL        8562
#define MSG_DECODE_1553B_SRC           8563

#define MSG_DECODE_1553B_POL           8564
#define MSG_DECODE_1553B_THRE1         8565
#define MSG_DECODE_1553B_THRE2         8566
#define MSG_DECODE_EVT_DATA            8567

#define MSG_DECODE_EVT_RX              8568
#define MSG_DECODE_EVT_TX              8569
#define MSG_DECODE_EVT_INFO            8570
#define MSG_DECODE_EVT_WR              8571

#define MSG_DECODE_EVT_ADDR            8572
#define MSG_DECODE_EVT_MISO            8573
#define MSG_DECODE_EVT_MOSI            8574
#define MSG_DECODE_EVT_ID              8575

#define MSG_DECODE_EVT_PARITY          8576
#define MSG_DECODE_EVT_CRC             8577
#define MSG_DECODE_EVT_IDFIELD         8578
#define MSG_DECODE_EVT_ERR             8579

#define MSG_DECODE_EVT_UART_DIR        8580
#define MSG_DECODE_EVT_RXERR           8581
#define MSG_DECODE_EVT_MISOERR         8582
#define MSG_DECODE_EVT_MOSIERR         8583

#define MSG_DECODE_EVT_DLC             8584
#define MSG_DECODE_EVT_TIME            8585
#define MSG_DECODE_EVT_ACK             8586
#define MSG_DECODE_EVT_PAL             8587

#define MSG_DECODE_EVT_RS232           8588
#define MSG_DECODE_EVT_SPI             8589
#define MSG_DECODE_EVT_I2C             8590
#define MSG_DECODE_EVT_LIN             8591

#define MSG_DECODE_EVT_CAN             8592
#define MSG_DECODE_EVT_I2S             8593
#define MSG_DECODE_EVT_FLEXRAY         8594
#define MSG_DECODE_EVT_1WIRE           8595

#define MSG_DECODE_EVT_SBUS            8596
#define MSG_DECODE_EVT_SENT            8597
#define MSG_DECODE_EVT_1553B           8598
#define MSG_DECODE_EVT_ARINC429        8599

#define MSG_DECODE_EVT_READ            8600
#define MSG_DECODE_EVT_WRITE           8601
#define MSG_DECODE_EVT_Y               8602
#define MSG_DECODE_EVT_N               8603

#define MSG_DECODE_EVT_STOP            8604
#define MSG_DECODE_EVT_START           8605
#define MSG_DECODE_EVT_RESTART         8606
#define MSG_DECODE_EVT_CHECK           8607

#define MSG_DECODE_EVT_FLEX_IND        8608
#define MSG_DECODE_EVT_FLEX_FID        8609
#define MSG_DECODE_EVT_FLEX_LEN        8610
#define MSG_DECODE_EVT_FLEX_HCRC       8611

#define MSG_DECODE_EVT_FLEX_CYC        8612
#define MSG_DECODE_EVT_FLEX_FCRC       8613
#define MSG_DECODE_EVT_LEFT            8614
#define MSG_DECODE_EVT_RIGHT           8615

#define MSG_DECODE_EVT_TYPE            8616
#define MSG_DECODE_EVT_PAYLOAD         8617
#define MSG_DECODE_EVT_CS              8618
#define IDS_DECODE1                    8619

#define IDS_DECODE2                    8620
#define IDS_DECODE3                    8621
#define IDS_DECODE4                    8622

// vdecodesel
#define MSG_APP_DECODESEL    8960
#define MSG_APP_DECODE1      8961
#define MSG_APP_DECODE2      8962
#define MSG_APP_DECODE3      8963

#define MSG_APP_DECODE4      8964

// storage
#define MSG_APP_STORAGE                9472
#define MSG_STORAGE_SAVE_IMAGE         9473
#define MSG_STORAGE_SAVE_WAVE          9474
#define MSG_STORAGE_SAVE_SCR           9475

#define MSG_STORAGE_SAVE_MEM           9476
#define MSG_STORAGE_SAVE_SETUP         9477
#define MSG_STORAGE_LOAD_WAVE          9478
#define MSG_STORAGE_LOAD_SETUP         9479

#define MSG_STORAGE_DISK_MANAGE        9480
#define MSG_STORAGE_OPTION             9481
#define MSG_STORAGE_PREFIX             9482
#define MSG_STORAGE_SAVE               9483

#define MSG_STORAGE_LOAD               9484
#define MSG_STORAGE_IMAGE_FORMAT       9485
#define MSG_STORAGE_IMAGE_INVERT       9486
#define MSG_STORAGE_IMAGE_COLOR        9487

#define MSG_STORAGE_IMAGE_HEADER       9488
#define MSG_STORAGE_DIALOG_VISIBLE     9489
#define MSG_STORAGE_IMAGE_FOOTER       9490
#define MSG_STORAGE_WAVE_DEPTH         9491

#define MSG_STORAGE_WAVE_FORMAT        9492
#define MSG_STORAGE_SCR_FORMAT         9493
#define MSG_STORAGE_OPTION_PARASAVE    9494
#define MSG_STORAGE_CHANNEL            9495

#define MSG_STORAGE_FILETYPE           9496
#define MSG_STORAGE_MEM_CSV_MORE       9497
#define MSG_STORAGE_MEM_CSV_TIME       9498
#define MSG_STORAGE_WFM_CHANNEL        9499

#define MSG_STORAGE_SUB_SAVE           9500
#define MSG_STORAGE_SUB_LOAD           9501
#define MSG_STORAGE_SECURITYCLEAR      9502
#define MSG_STORAGE_NEWFILE            9503

#define MSG_STORAGE_NEWFOLDER          9504
#define MSG_STORAGE_DELETE             9505
#define MSG_STORAGE_RENAME             9506
#define MSG_STORAGE_COPY               9507

#define MSG_STORAGE_PASTE              9508
#define MSG_STORAGE_WAVE_SIZE          9509
#define MSG_STORAGE_AUTONAME           9510
#define MSG_STORAGE_HIDDEN             9511

#define MSG_STORAGE_FILENAME           9512
#define MSG_STORAGE_PATHNAME           9513
#define MSG_STORAGE_FILEPATH           9514
#define MSG_STORAGE_RESULT             9515

#define MSG_STORAGE_USB_INSERT         9516
#define MSG_STORAGE_USB_REMOVE         9517
#define MSG_CHECKING_FIRMWARE          9518
#define MSG_DOWNLOAD_FIRMWARE          9519

#define MSG_STORAGE_PRNTSCR            9520
#define MSG_RESTORE_DEFAULT            9521
#define MSG_STORAGE_PULLSCR            9522

// source
#define MSG_APP_DG                   9984
#define MSG_DG_WAVE                  9985
#define MSG_DG_FREQ_PERIOD           9986
#define MSG_DG_FREQ_PERIOD_SUB_1     9987

#define MSG_DG_AMP_LEVEL             9988
#define MSG_DG_AMP_LEVEL_SUB_1       9989
#define MSG_DG_OFFSET_LEVEL          9990
#define MSG_DG_OFFSET_LEVEL_SUB_1    9991

#define MSG_DG_OFFSET                9992
#define MSG_DG_STARTPHASE            9993
#define MSG_DG_SYNCPAHSE             9994
#define MSG_DG_SETUP_NORM            9995

#define MSG_DG_SETUP_RAMP            9996
#define MSG_DG_SETUP_PULSE           9997
#define MSG_DG_DUTY_CYCLE            9998
#define MSG_DG_SYMMETRY              9999

#define MSG_DG_IMPEDANCE             10000
#define MSG_DG_MOD_BUR_SWEEP         10001
#define MSG_DG_BURST_TYPE            10002
#define MSG_DG_BURST_CYCLE_NUM       10003

#define MSG_DG_TRIGSRC               10004
#define MSG_DG_MANUAL_TRIG           10005
#define MSG_DG_BURST_PERIOD          10006
#define MSG_DG_BURST_DELAY           10007

#define MSG_DG_BURST_MORE            10008
#define MSG_DG_SWEEP_TYPE            10009
#define MSG_DG_SWEEP_TIME            10010
#define MSG_DG_SWEEP_BACKTIME        10011

#define MSG_DG_SWEEP_STARTFREQ       10012
#define MSG_DG_SWEEP_ENDFREQ         10013
#define MSG_DG_SWEEP_MORE            10014
#define MSG_DG_SWEEP_STARTKEEP       10015

#define MSG_DG_SWEEP_ENDKEEP         10016
#define MSG_DG_SWEEP_STEP            10017
#define MSG_DG_MOD_TYPE              10018
#define MSG_DG_MOD_TYPE_RAMP         10019

#define MSG_DG_MOD_FREQ              10020
#define MSG_DG_MOD_FSK_POLARITY      10021
#define MSG_DG_MOD_SHAPE             10022
#define MSG_DG_MOD_AM_DEPTH          10023

#define MSG_DG_MOD_FM_DEV            10024
#define MSG_DG_MOD_FSK_HOP           10025
#define MSG_DG_MOD_FSK_RATE          10026
#define MSG_DG_SPACE                 10027

#define MSG_DG_ARB_STORED            10028
#define MSG_DG_ARB_CREATE            10029
#define MSG_DG_ARB_EDIT              10030
#define MSG_DG_ARB_SAVE              10031

#define MSG_DG_ARB_SETUP             10032
#define MSG_DG_ARB_CHAN              10033
#define MSG_DG_ARB_CHAN_SELECT       10034
#define MSG_DG_ARB_REGION            10035

#define MSG_DG_ARB_LOAD              10036
#define MSG_DG_ARB_CURSORA           10037
#define MSG_DG_ARB_CURSORB           10038
#define MSG_DG_ARB_CURSORAB          10039

#define MSG_DG_ARB_CURRENT           10040
#define MSG_DG_ARB_ZOOM              10041
#define MSG_DG_ARB_LINEAR            10042
#define MSG_DG_ARB_POINTS            10043

#define MSG_DG_ARB_CREATE_MENU       10044
#define MSG_DG_ARB_EDIT_MENU         10045
#define MSG_DG_ARB_DONE              10046
#define MSG_DG_ARB_VOLTAGE           10047

#define MSG_DG_ARB_INSERT            10048
#define MSG_DG_ARB_DELETE            10049
#define MSG_DG_ARB_TIME              10050
#define IDS_SOURCE1                  10051

#define IDS_SOURCE2                  10052

// record
#define MSG_APP_RECORD             10496
#define MSG_RECORD_ONOFF           10497
#define MSG_RECORD_START           10498
#define MSG_RECORD_PLAY            10499

#define MSG_RECORD_OPTION          10500
#define MSG_RECORD_MORE            10501
#define MSG_RECORD_CURRENT         10502
#define MSG_RECORD_INTERVAL        10503

#define MSG_RECORD_FRAMES          10504
#define MSG_RECORD_MAXFRAMES       10505
#define MSG_RECORD_SET2MAX         10506
#define MSG_RECORD_BEEPER          10507

#define MSG_RECORD_PLAYMODE        10508
#define MSG_RECORD_PLAYDIR         10509
#define MSG_RECORD_PLAYINTERVAL    10510
#define MSG_RECORD_STARTFRAME      10511

#define MSG_RECORD_ENDFRAME        10512
#define MSG_RECORD_FRAMESTART      10513
#define MSG_RECORD_FROMFRAME       10514
#define MSG_RECORD_TOFRAME         10515

#define MSG_RECORD_SAVE            10516
#define MSG_RECORD_SAVE2FILE       10517
#define MSG_REC_KEY_MENU           10518
#define MSG_RECORD_SPACE           10519

#define MSG_REC_PLAY_BACK          10520
#define MSG_REC_PLAY_NEXT          10521
#define MSG_REC_PLAY_STOP          10522
#define IDS_RECORD_EXIT            10523

#define IDS_CURRENT_CHANGED        10524

// la
#define MSG_LA_ENABLE                 11008
#define MSG_LA_CURRENT_CHAN           11009
#define MSG_LA_POS                    11010
#define MSG_LA_POSITION               11011

#define MSG_LA_CH_ONOFF               11012
#define MSG_LA_THRE_SET               11013
#define MSG_LA_AUTO_SET               11014
#define MSG_LA_WAVE_SIZE              11015

#define MSG_LA_LABEL_SET              11016
#define MSG_LA_OTHER_SET              11017
#define MSG_LA_SELECT_CHAN            11018
#define MSG_LA_SELECT_GROUP           11019

#define MSG_LA_D0D7_ONOFF             11020
#define MSG_LA_D8D15_ONOFF            11021
#define MSG_LA_LOW_THRE_VAL           11022
#define MSG_LA_LOW_THRE_VAL_SUB_1     11023

#define MSG_LA_HIGH_THRE_VAL          11024
#define MSG_LA_HIGH_THRE_VAL_SUB_1    11025
#define MSG_LA_PRESET_LABEL           11026
#define MSG_LA_LABEL_SELECT_CHAN      11027

#define MSG_LA_LABEL_VIEW             11028
#define MSG_LA_INPUT_LABEL            11029
#define MSG_LA_GROUP_SET              11030
#define MSG_LA_DELAY_CAL              11031

#define MSG_LA_GROUP1_SET             11032
#define MSG_LA_GROUP2_SET             11033
#define MSG_LA_GROUP3_SET             11034
#define MSG_LA_GROUP4_SET             11035

#define MSG_LA_UN_GROUP               11036
#define MSG_LA_INVISIBLE_TITLE        11037
#define MSG_LA_COLOR_SET              11038
#define MSG_LA_COLOR                  11039

#define MSG_LA_HIGH_COLOR             11040
#define MSG_LA_EDGE_COLOR             11041
#define MSG_LA_LOW_COLOR              11042
#define MSG_LA_THRE_CAL               11043

#define IDS_SIZE_CHANGED              11044
#define IDS_SELECT_CHANGED            11045
#define IDS_POS_CHANGED               11046
#define IDS_LA_PROBE_CONNECT          11047

#define IDS_LA_PROBE_DISCONNECT       11048
#define IDS_LA_OPTION_EXPIRED         11049
#define IDS_LA_OPTION_MISSING         11050
#define IDS_LA_DISABLE_IN_XY          11051


// display
#define MSG_APP_DISPLAY               11520
#define MSG_DISPLAY_TYPE              11521
#define MSG_DISPLAY_PERSISTIME        11522
#define MSG_DISPLAY_WAVE_INTENSITY    11523

#define MSG_DISPLAY_GRID              11524
#define MSG_DISPLAY_GRID_INTENSITY    11525
#define MSG_DISPLAY_RULERS            11526
#define MSG_DISPLAY_GRID_CHANGED      11527

#define MSG_DISPLAY_10X8_10X10        11528
#define MSG_DISPLAY_CLEAR             11529
#define MSG_DISPLAY_PALETTE           11530
#define MSG_DISPLAY_MORE              11531

#define MSG_DISPLAY_FREEZE            11532

// utility
#define MSG_APP_UTILITY_MENU                  12032
#define MSG_APP_UTILITY_IOSET_MENU            12033
#define MSG_APP_UTILITY_BEEPER                12034
#define MSG_APP_UTILITY_LANGUAGE              12035

#define MSG_MASK_MENU                         12036
#define MSG_APP_UTILITY_RECODER_MENU          12037
#define MSG_APP_UTILITY_SYSTEM_MENU           12038
#define MSG_APP_UTILITY_MORE_MENU             12039

#define MSG_APP_UTILITY_HELP_MENU             12040
#define MSG_APP_UTILITY_AUXOUT                12041
#define MSG_APP_UTILITY_AUTO_OPT              12042
#define MSG_APP_UTILITY_PRINT_MENU            12043

#define MSG_APP_UTILITY_EMAIL_MENU            12044
#define MSG_APP_UTILITY_SELFCAL_MENU          12045
#define MSG_APP_UTILITY_CHECK_MENU            12046
#define MSG_APP_UTILITY_KEYBOARD_MENU         12047

#define MSG_APP_UTILITY_LOCK_AUTO             12048
#define MSG_APP_UTILITY_SYSTEMINFO            12049
#define MSG_APP_UTILITY_POWER_ON_SET          12050
#define MSG_APP_UTILITY_POWER_STATUS          12051

#define MSG_APP_UTILITY_LOCK_KB               12052
#define MSG_APP_UTILITY_LOCK_KEYS             12053
#define MSG_APP_UTILITY_SCREEN_SAVER          12054
#define MSG_APP_UTILITY_SCREEN_SELECT         12055

#define MSG_APP_UTILITY_SCREEN_WORD           12056
#define MSG_APP_UTILITY_SCREEN_PICTURE        12057
#define MSG_APP_UTILITY_SCREEN_PREVIEW        12058
#define MSG_APP_UTILITY_SCREEN_TIME           12059

#define MSG_APP_UTILITY_SCREEN_DEFAULT        12060
#define MSG_APP_UTILITY_TEST_MENU             12061
#define MSG_APP_UTILITY_KEY_TEST              12062
#define MSG_APP_UTILITY_TOUCH_TEST            12063

#define MSG_APP_UTILITY_SCREEN_TEST           12064
#define MSG_APP_UTILITY_FAN_TEST              12065
#define MSG_APP_UTILITY_SELF_TEST             12066
#define MSG_APP_UTILITY_CHECK_MORE_MENU       12067

#define MSG_APP_UTILITY_RST_STARTTIMES        12068
#define MSG_APP_UTILITY_RESET_LIVE_TIME       12069
#define MSG_APP_UTILITY_RESET_PREVATE_DATA    12070
#define MSG_TOUCH_ENABLE                      12071

#define MSG_APP_UTILITY_QUICK_MENU            12072
#define MSG_APP_UTILITY_PROJECT               12073
#define MSG_APP_UTILITY_SHOW_TIME             12074
#define MSG_APP_UTILITY_TIME                  12075

#define MSG_APP_UTILITY_YEAR                  12076
#define MSG_APP_UTILITY_MONTH                 12077
#define MSG_APP_UTILITY_DAY                   12078
#define MSG_APP_UTILITY_HOUR                  12079

#define MSG_APP_UTILITY_MINUTE                12080
#define MSG_APP_UTILITY_SECOND                12081
#define MSG_APP_UTILITY_APPLY_TIME            12082
#define MSG_APP_UTILITY_RST_SPACE             12083

#define MSG_APP_UTILITY_RECORD_KEY            12084
#define MSG_APP_UTILITY_RESTART               12085
#define MSG_APP_UTILITY_SHUTDOWN              12086
#define MSG_APP_UTILITY_REF_CLOCK             12087

#define MSG_APP_UTILITY_MORE_MORE             12088
#define MSG_APP_UTILITY_DEFAULT_OPTION        12089
#define MSG_APP_UTILITY_DEFAULT_IMP           12090

// ioset
#define MSG_APP_UTILITY_IO_MENU          12544
#define MSG_APP_UTILITY_LXI_MENU         12545
#define MSG_APP_UTILITY_WIFI_MENU        12546
#define MSG_APP_UTILITY_USBTMC_MENU      12547

#define MSG_APP_UTILITY_GPIB_MENU        12548
#define MSG_APP_UTILITY_LXI_CFG          12549
#define MSG_APP_UTILITY_LXI_APPLY        12550
#define MSG_APP_UTILITY_LXI_INIT         12551

#define MSG_APP_UTILITY_LXI_IPADDRESS    12552
#define MSG_APP_UTILITY_LXI_SUBNET       12553
#define MSG_APP_UTILITY_LXI_GATEWAY      12554
#define MSG_APP_UTILITY_LXI_DNS          12555

#define MSG_APP_UTILITY_LXI_MDNS         12556
#define MSG_APP_UTILITY_LXI_HOST_NAME    12557
#define MSG_APP_UTILITY_HDMI_MENU        12558
#define MSG_APP_UTILITY_HDMI             12559

#define MSG_APP_UTILITY_HDMI_RES         12560
#define MSG_APP_UTILITY_HDMI_EDID        12561

// email
#define MSG_APP_EMAIL                13056
#define MSG_EMAIL_RECEIVER           13057
#define MSG_EMAIL_ATTACHMENT         13058
#define MSG_EMAIL_ATTACHMENT_USER    13059

#define MSG_EMAIL_SEND               13060
#define MSG_EMAIL_SETTING            13061
#define MSG_EMAIL_USERNAME           13062
#define MSG_EMAIL_PASSWORD           13063

#define MSG_EMAIL_SMTP_IP            13064
#define MSG_EMAIL_SMTP_PORT          13065
#define MSG_EMAIL_TEST               13066
#define MSG_EMAIL_APPLY              13067

#define MSG_EMAIL_DEFAULT            13068
#define MSG_EMAIL_SPACE              13069

// print
#define MSG_APP_PRINTER           13568
#define MSG_PRINTER_START         13569
#define MSG_PRINTER_RANGE         13570
#define MSG_PRINTER_PALETTE       13571

#define MSG_PRINTER_PAPER_SIZE    13572
#define MSG_PRINTER_COPIES        13573
#define MSG_PRINTER_INVERT        13574
#define MSG_PRINTER_SET_MENU      13575

#define MSG_PRINTER_VENDOR        13576
#define MSG_PRINTER_IP            13577
#define MSG_PRINTER_PORT          13578
#define MSG_PRINTER_LINK_TEST     13579

#define MSG_PRINTER_TEST_PAGE     13580

// wifi
#define MSG_APP_UTILITY_WIFI              14080
#define MSG_APP_UTILITY_WIFI_OPENCLOSE    14081
#define MSG_APP_UTILITY_WIFI_SCAN         14082
#define MSG_APP_UTILITY_WIFI_NAME         14083

#define MSG_APP_UTILITY_WIFI_PWD          14084
#define MSG_APP_UTILITY_WIFI_CONNECT      14085
#define MSG_APP_UTILITY_WIFI_RETURN       14086
#define MSG_APP_UTILITY_WIFI_KEY          14087


// search
#define MSG_SEARCH_APP                     14592
#define MSG_SEARCH_EN                      14593
#define MSG_SEARCH_TYPE                    14594
#define MSG_SEARCH_EDGE_SETUP              14595

#define MSG_SEARCH_PULSE_SETUP             14596
#define MSG_SEARCH_RUNT_SETUP              14597
#define MSG_SEARCH_SLOPE_SETUP             14598
#define MSG_SEARCH_RS232_SETUP             14599

#define MSG_SEARCH_IIC_SETUP               14600
#define MSG_SEARCH_SPI_SETUP               14601
#define MSG_SEARCH_NAVIGATION_EVENT        14602
#define MSG_SEARCH_MARK_TABEL_EN           14603

#define MSG_SEARCH_MORE                    14604
#define MSG_SEARCH_CPY_TO_TRIGGER          14605
#define MSG_SEARCH_CPY_FROM_TRIGGER        14606
#define MSG_SEARCH_MARK_TABEL_SAVE         14607

#define MSG_SEARCH_THRE_ONE                14608
#define MSG_SEARCH_THRE                    14609
#define MSG_SEARCH_THRE_DOUBLE             14610
#define MSG_SEARCH_THRE_A                  14611

#define MSG_SEARCH_THRE_B                  14612
#define MSG_SEARCH_THRE_IIC                14613
#define MSG_SEARCH_THRE_IIC_SCL            14614
#define MSG_SEARCH_THRE_IIC_SDA            14615

#define MSG_SEARCH_THRE_SPI                14616
#define MSG_SEARCH_THRE_SPI_SCL            14617
#define MSG_SEARCH_THRE_SPI_SDA            14618
#define MSG_SEARCH_THRE_SPI_CS             14619

#define MSG_SEARCH_SPACE                   14620
#define MSG_SEARCH_PLAY_PRE                14621
#define MSG_SEARCH_PLAY_NEXT               14622
#define MSG_SEARCH_PLAY_STOP               14623

#define MSG_SEARCH_EDGE_APP                14624
#define MSG_SEARCH_EDGE_SOURCE             14625
#define MSG_SEARCH_EDGE_SLOPE              14626
#define MSG_SEARCH_PULSE_APP               14627

#define MSG_SEARCH_PULSE_SOURCE            14628
#define MSG_SEARCH_PULSE_POLARITY          14629
#define MSG_SEARCH_PULSE_WHEN              14630
#define MSG_SEARCH_PULSE_UPPER             14631

#define MSG_SEARCH_PULSE_LOWER             14632
#define MSG_SEARCH_RUNT_SETUP_APP          14633
#define MSG_SEARCH_RUNT_SOURCE             14634
#define MSG_SEARCH_RUNT_POLARITY           14635

#define MSG_SEARCH_RUNT_QUALIFIER          14636
#define MSG_SEARCH_RUNT_UPPER              14637
#define MSG_SEARCH_RUNT_LOWER              14638
#define MSG_SEARCH_SLOPE_SETUP_APP         14639

#define MSG_SEARCH_SLOPE_SOURCE            14640
#define MSG_SEARCH_SLOPE_POLARITY          14641
#define MSG_SEARCH_SLOPE_WHEN              14642
#define MSG_SEARCH_SLOPE_UPPER             14643

#define MSG_SEARCH_SLOPE_LOWER             14644
#define MSG_SEARCH_RS232_SETUP_APP         14645
#define MSG_SEARCH_RS232_SOURCE            14646
#define MSG_SEARCH_RS232_POLARITY          14647

#define MSG_SEARCH_RS232_BAUDRATE          14648
#define MSG_SEARCH_RS232_BAUDRATE_SUB_1    14649
#define MSG_SEARCH_RS232_WHEN              14650
#define MSG_SEARCH_RS232_DATAWIDTH         14651

#define MSG_SEARCH_RS232_CHECK             14652
#define MSG_SEARCH_RS232_STOPBIT           14653
#define MSG_SEARCH_RS232_DATA              14654
#define MSG_SEARCH_IIC_SETUP_APP           14655

#define MSG_SEARCH_I2C_SCL                 14656
#define MSG_SEARCH_I2C_SDA                 14657
#define MSG_SEARCH_I2C_WHEN                14658
#define MSG_SEARCH_I2C_ADDRTYPE            14659

#define MSG_SEARCH_I2C_ADDRDATA_BITS       14660
#define MSG_SEARCH_I2C_ADDRESS             14661
#define MSG_SEARCH_I2C_BYTELENGTH          14662
#define MSG_SEARCH_I2C_DATA                14663

#define MSG_SEARCH_I2C_DIRECTION           14664
#define MSG_SEARCH_ADDRDATA_SETUP          14665
#define MSG_SEARCH_SPI_SETUP_APP           14666
#define MSG_SEARCH_SPI_SCL                 14667

#define MSG_SEARCH_SPI_SDA                 14668
#define MSG_SEARCH_SPI_CS                  14669
#define MSG_SEARCH_SPI_TIMEOUT             14670
#define MSG_SEARCH_SPI_WHEN                14671

#define MSG_SEARCH_SPI_CSMODE              14672
#define MSG_SEARCH_SPI_DATA_MENU           14673
#define MSG_SEARCH_SPI_DATABITS            14674
#define MSG_SEARCH_SPI_DATA                14675

#define MSG_SEARCH_SPI_MORE                14676
#define CMD_SEARCH_GET_DATA                14677
#define CMD_SEARCH_SOURCE_CURR_PTR         14678
#define CMD_SEARCH_CODE_ALL                14679

#define CMD_SEARCH_CODE_CURR_BIT           14680
#define CMD_SEARCH_CODE_VALUE              14681
#define CMD_SEARCH_CODE_ARG                14682
#define STRING_SEARCH_TOTAL_COUNT          14683

#define STRING_SEARCH_NUMBER               14684
#define STRING_SEARCH_TIME                 14685
#define STRING_SEARCH_COUNT                14686
#define MSG_SEARCH_KEY_MENU                14687


// quick
#define MSG_APP_QUICK                15104
#define MSG_QUICK_OPERATION          15105
#define MSG_QUICK_SETTING            15106
#define MSG_QUICK_IMAGE_FORMAT       15107

#define MSG_QUICK_PREFIX             15108
#define MSG_QUICK_IMAGE_INVERT       15109
#define MSG_QUICK_IMAGE_COLOR        15110
#define MSG_QUICK_WAVE_SCR_FORMAT    15111

#define MSG_QUICK_WAVE_MEM_FORMAT    15112
#define MSG_QUICK_CHANNEL            15113
#define MSG_QUICK_WAVE_SIZE          15114
#define MSG_QUICK_CSV_SEQUENCE       15115

#define MSG_QUICK_WAVE_FROM          15116
#define MSG_QUICK_MEAS_ALL_SRC       15117
#define MSG_QUICK_STAT_RESET         15118
#define MSG_QUICK_SELECT_SAVE        15119


// zonetrigger
#define MSG_APP_TRIG_ZONE_MENU       15616
#define MSG_TRIG_ZONE_A_ENABLE       15617
#define MSG_TRIG_ZONE_A_SOURCE       15618
#define MSG_TRIG_ZONE_A_INTERSECT    15619

#define MSG_TRIG_ZONE_B_ENABLE       15620
#define MSG_TRIG_ZONE_B_SOURCE       15621
#define MSG_TRIG_ZONE_B_INTERSECT    15622
#define CMD_ZONE_TOUCH_ENABLE        15623

#define CMD_TRIG_ZONE_UPDATE_SHOW    15624
#define STRING_TRIG_ZONE_A           15625
#define STRING_TRIG_ZONE_B           15626
#define STRING_HISTO_ZONE            15627

#define STRING_HORI_ZOOM_IN          15628
#define STRING_VERT_ZOOM_IN          15629
#define STRING_WAVE_ZOOM_IN          15630
#define STRING_HORI_ZOOM_OUT         15631

#define STRING_VERT_ZOOM_OUT         15632
#define STRING_WAVE_ZOOM_OUT         15633

// carrier
#define MSG_APP_CARRIER     16128
#define MSG_CARRIER_EN      16129
#define MSG_CARRIER_TIME    16130
#define MSG_CARRIER_FREQ    16131


// menutest
#define MSG_MENUTEST_CONTROL                                     16640
#define MSG_MENUTEST_CONTROL_COMBOX_FUNC                         16641
#define MSG_MENUTEST_CONTROL_BUTTON_TWO                          16642
#define MSG_MENUTEST_CONTROL_BUTTON_ONE                          16643

#define MSG_MENUTEST_CONTROL_BUTTON_SUB                          16644
#define MSG_MENUTEST_CONTROL_OPT_C123                            16645
#define MSG_MENUTEST_CONTROL_OPT_A1                              16646
#define MSG_MENUTEST_CONTROL_LABEL_FLOAT                         16647

#define MSG_MENUTEST_CONTROL_LABEL_INT_FLOAT_FLOAT_KNOB          16648
#define MSG_MENUTEST_CONTROL_LABEL_INT_FLOAT_FLOAT_KNOB_SUB_1    16649
#define MSG_MENUTEST_CONTROL_COMBOX                              16650
#define MSG_MENUTEST_CONTROL_LABEL_ICON                          16651

#define MSG_MENUTEST_CONTROL_CHECKBOX                            16652
#define MSG_MENUTEST_CONTROL_LABEL_INT                           16653
#define MSG_MENUTEST_CONTROL_LABEL_STR                           16654
#define MSG_MENUTEST_CONTROL_BUTTON_SUB_NO_RET                   16655

#define MSG_MENUTEST_CONTROL_LABEL_IME                           16656
#define MSG_MENUTEST_SUBMENU                                     16657
#define MSG_MENUTEST_SUBMENU_SUBMENU                             16658
#define MSG_MENUTEST_CHILDMENU1                                  16659

#define MSG_MENUTEST_CHILDMENU1_BUTTON_TWO                       16660
#define MSG_MENUTEST_CHILDMENU1_COMBOX_MENU                      16661
#define MSG_MENUTEST_CHILDMENU2                                  16662
#define MSG_MENUTEST_CHILDMENU3                                  16663

#define MSG_MENUTEST_A1                                          16664
#define MSG_MENUTEST_A1_A                                        16665
#define MSG_MENUTEST_A1_B                                        16666
#define MSG_MENUTEST_A2                                          16667

#define MSG_MENUTEST_A2_F                                        16668
#define MSG_MENUTEST_B1                                          16669
#define MSG_MENUTEST_B1_C                                        16670
#define MSG_MENUTEST_B2                                          16671

#define MSG_MENUTEST_B2_D                                        16672
#define MSG_MENUTEST_B3                                          16673
#define MSG_MENUTEST_B3_E                                        16674
#define MSG_MENUTEST_F1                                          16675

#define MSG_MENUTEST_F1_G                                        16676
#define MSG_MENUTEST_F2                                          16677
#define MSG_MENUTEST_F2_H                                        16678
#define MSG_MENUTEST_CONTROL_COMBOX_KNOB                         16679

#define MSG_MENUTEST_CONTROL_COMBOX_KNOB_SUB_1                   16680
#define MSG_MENUTEST_CONTROL_TIME_EDIT                           16681
#define MSG_MENUTEST_CONTROL_DATE_EDIT                           16682
#define MSG_MENUTEST_CONTROL_IP_EDIT                             16683

#define MSG_MENUTEST_CONTROL_SEEK_BAR                            16684
#define MSG_MENUTEST_CONTROL_COMBOX_KNOB_ICON                    16685
#define MSG_MENUTEST_CONTROL_COMBOX_KNOB_ICON_SUB_1              16686
#define MSG_MENUTEST_CONTROL_BUTTON_SUB2                         16687

#define MSG_MENUTEST_CONTROL_COMBOX_KNOB_CHILD                   16688
#define MSG_MENUTEST_CONTROL_COMBOX_KNOB_CHILD_SUB_1             16689
#define MSG_MENUTEST_CONTROL_BUTTON_ONE1                         16690
#define MSG_MENUTEST_CONTROL_BUTTON_ONE2                         16691

#define MSG_MENUTEST_CONTROL_BUTTON_ONE3                         16692
#define MSG_MENUTEST_CONTROL_BUTTON_ONE4                         16693
#define MSG_MENUTEST_CONTROL_BUTTON_ONE5                         16694
#define MSG_MENUTEST_CONTROL_BUTTON_ONE6                         16695

#define MSG_MENUTEST_CONTROL_BUTTON_ONE7                         16696

// help
#define MSG_APP_HELP              17152
#define MSG_APP_HELP_ABOUT        17153
#define MSG_APP_HELP_LANGUAGE     17154
#define MSG_APP_HELP_OPTION       17155

#define MSG_APP_HELP_SETUP_OPT    17156
#define MSG_APP_HELP_HELP         17157
#define MSG_APP_HELP_NONE         17158
#define MSG_APP_HELP_CHECK_NEW    17159

#define MSG_APP_HELP_UPGRADE      17160
#define MSG_OPT_INFO_BW70T100     17161
#define MSG_OPT_INFO_BW70T200     17162
#define MSG_OPT_INFO_BW70T350     17163

#define MSG_OPT_INFO_BW1T2        17164
#define MSG_OPT_INFO_BW1T3        17165
#define MSG_OPT_INFO_BW1T5        17166
#define MSG_OPT_INFO_BW2T3        17167

#define MSG_OPT_INFO_BW2T5        17168
#define MSG_OPT_INFO_BW3T5        17169
#define MSG_OPT_INFO_BW6T10       17170
#define MSG_OPT_INFO_BW6T20       17171

#define MSG_OPT_INFO_BW10T20      17172
#define MSG_OPT_INFO_MSO          17173
#define MSG_OPT_INFO_2RL          17174
#define MSG_OPT_INFO_200RL        17175

#define MSG_OPT_INFO_5RL          17176
#define MSG_OPT_INFO_BND          17177
#define MSG_OPT_INFO_COMP         17178
#define MSG_OPT_INFO_EMBD         17179

#define MSG_OPT_INFO_AUTO         17180
#define MSG_OPT_INFO_FLEX         17181
#define MSG_OPT_INFO_AUDIO        17182
#define MSG_OPT_INFO_SENSOR       17183

#define MSG_OPT_INFO_AERO         17184
#define MSG_OPT_INFO_ARINC        17185
#define MSG_OPT_INFO_DG           17186
#define MSG_OPT_INFO_JITTER       17187

#define MSG_OPT_INFO_MASK         17188
#define MSG_OPT_INFO_PWR          17189
#define MSG_OPT_INFO_DVM          17190
#define MSG_OPT_INFO_CTR          17191

#define MSG_OPT_INFO_EDK          17192
#define MSG_OPT_INFO_4CH          17193
#define MSG_OPT_HELP_INFO         17194
#define MSG_OPT_HELP_TITLE        17195

#define MSG_OPT_HELP_CONT         17196

// histogram
#define MSG_HISTO                17664
#define MSG_HISTO_EN             17665
#define MSG_HISTO_TYPE           17666
#define MSG_HISTO_SOURCE         17667

#define MSG_HISTO_MEASUREMENT    17668
#define MSG_HISTO_DISPGRID       17669
#define MSG_HISTO_STATISEN       17670
#define MSG_HISTO_RESET          17671

#define MSG_HISTO_RANGE          17672
#define MSG_HISTO_LEFTPOS        17673
#define MSG_HISTO_RIGHTPOS       17674
#define MSG_HISTO_HIGHPOS        17675

#define MSG_HISTO_LOWPOS         17676

// guiserve
#define MSG_GUI_MENU_ASK                 18176
#define MSG_GUI_SERV_ASK_OK              18177
#define MSG_GUI_SERV_ASK_CANCEL          18178
#define MSG_GUI_MENU_CANCEL              18179

#define MSG_GUI_SERV_CANCEL              18180
#define MSG_GUI_NOTIFY_KEY_INFO          18181
#define MSG_GUI_MENU_SHOW                18182
#define ERR_INVALID_INPUT                18183

#define ERR_INVALID_CONFIG               18184
#define ERR_OVER_RANGE                   18185
#define ERR_OVER_LOW_RANGE               18186
#define ERR_OVER_UPPER_RANGE             18187

#define ERR_MEMORY_FAIL                  18188
#define ERR_OVER_ACCESS                  18189
#define ERR_ACTION_DISABLED              18190
#define ERR_IME_LENGTH_OVER              18191

#define ERR_SERV_GUI_INSTALL_NOTIFY      18192
#define ERR_SERV_GUI_UNINSTALL_NOTIFY    18193
#define ERR_SERV_GUI_NOTIFY_SPY          18194
#define ERR_TYPE_MIS_MATCH               18195

#define ERR_TARGET_MIS_MATCH             18196
#define ERR_TARGET_INVALID_CFG           18197
#define ERR_SERV_ID_CONFLICT             18198
#define ERR_SERV_NAME_INVALID            18199

#define ERR_SERV_CREATED                 18200
#define ERR_SERV_NULL                    18201
#define ERR_NO_EXECUTOR                  18202
#define ERR_MSG_DO_FAIL                  18203

#define ERR_MSG_GET_FAIL                 18204
#define ERR_MSG_TYPE_MISMATCH            18205
#define ERR_IN_READ                      18206
#define ERR_IN_WRITE                     18207

#define ERR_IN_COMMUNICATE               18208
#define ERR_CONVERT_FAIL                 18209
#define ERR_DIV_0                        18210
#define ERR_MIS_MATCH                    18211

#define ERR_INVALID_CONST                18212
#define ERR_NULL_PTR                     18213
#define ERR_INVALID_RANGE_CONST          18214
#define ERR_QUICK_KEY_EXIST              18215

#define ERR_REGISTER_CMD                 18216
#define ERR_TCP_SERVER_LISTEN            18217
#define ERR_IN_DO_CMD                    18218
#define ERR_INVALID_CONTEXT              18219

#define ERR_NULL_IN_PROVIDER             18220
#define ERR_NO_TRACE                     18221
#define PHY_PHY_TRACE_MAP_FAIL           18222
#define ERR_SERICE_ACTIVE_NOW            18223

#define ERR_SERICE_ACTIVE_FAIL           18224
#define ERR_CAL_HIZ_AMP                  18225
#define ERR_CAL_DAC                      18226
#define ERR_CAL_FAIL_DATA                18227

#define ERR_CAL_HZ_GAIN                  18228
#define ERR_CAL_LZ_GAIN                  18229
#define ERR_CAL_GAIN_DISPATCH            18230
#define ERR_CAL_OFFSET_FAIL              18231

#define ERR_CAL_WRITE_FAIL               18232
#define ERR_CAL_READ_FAIL                18233
#define ERR_CAL_VERT                     18234
#define ERR_CAL_HORI                     18235

#define ERR_CAL_ADC                      18236
#define ERR_CAL_EXT                      18237
#define ERR_CAL_IDELAY_WND               18238
#define ERR_CAL_ADC_GAIN                 18239

#define ERR_CAL_ADC_OFFSET               18240
#define ERR_CAL_ADC_LINEAR               18241
#define ERR_CH_FILTER_MISS               18242
#define ERR_CH_HZ_GAIN_DISPATCH          18243

#define ERR_CH_LZ_GAIN_DISPATCH          18244
#define ERR_CH_HZ_TRIM_DISPATCH          18245
#define ERR_HORI_SCALE                   18246
#define ERR_MATH_RESAMPLE_FAIL           18247

#define ERR_CURSOR_NO_REF_RANGE          18248
#define ERR_PTHREAD_CREATE_FAIL          18249
#define ERR_PTHREAD_ISRUNNING            18250
#define ERR_LINUX_CMD_EXECUTE            18251

#define ERR_LAN_NOLINK                   18252
#define ERR_IP_CONFLICT                  18253
#define ERR_WIFI_NOREADY                 18254
#define ERR_WIFI_PWD_NULL                18255

#define ERR_EMAIL_SEND_TIMEOUT           18256
#define ERR_NULL_ADDRESSEE               18257
#define ERR_FILE_OP_FAIL                 18258
#define ERR_PRINTER_ABORT                18259

#define ERR_SOCKET_LINK_TIMEOUT          18260
#define ERR_SOCKET_OP_FAIL               18261
#define ERR_SOCKET_IOCTL_ERROR           18262
#define ERR_PRINTER_TIMEOUIT             18263

#define ERR_PRINTER_NETWORK              18264
#define ERR_PRINTER_BUSY                 18265
#define ERR_SMTP_ADDR_NULL               18266
#define ERR_SMTP_LINK_FAIL               18267

#define ERR_KEYBOARD_LOCKED              18268
#define ERR_THE_KEY_LOCKED               18269
#define ERR_THE_KEY_UNLOCKED             18270
#define ERR_BIN_SETUP_SAVE_FAIL          18271

#define ERR_BIN_SETUP_READ_FAIL          18272
#define ERR_BIN_SETUP_CONTENT            18273
#define ERR_BIN_SETUP_SERV_ID            18274
#define ERR_FILE_MAGIC                   18275

#define ERR_FILE_OLD_VER                 18276
#define ERR_FILE_NEW_VER                 18277
#define ERR_FILE_TYPE                    18278
#define ERR_FILE_MODEL                   18279

#define ERR_FILE_EXIST                   18280
#define ERR_FILE_SAVE_OK                 18281
#define ERR_FILE_SAVE_FAIL               18282
#define ERR_FILE_DELETE_OK               18283

#define ERR_FILE_DELETE_FAIL             18284
#define ERR_FILE_RENAME_OK               18285
#define ERR_FILE_RENAME_FAIL             18286
#define ERR_FILE_COPY_OK                 18287

#define ERR_FILE_COPY_FAIL               18288
#define ERR_FILE_NOT_EXIST               18289
#define ERR_FILE_INVALID_NAME            18290
#define ERR_FILE_CREATE_DIR              18291

#define ERR_MENU_JUMP_INVALID            18292
#define ERR_DEVICE_OP_FAIL               18293
#define ERR_SPI_OPEN_FAIL                18294
#define ERR_SPI_CFG_FAIL                 18295

#define ERR_AXI_OPEN_FAIL                18296
#define ERR_SYS_VEND_CONF_LOAD_FAIL      18297
#define ERR_SYS_VEND_CONF_SAVE_FAIL      18298
#define ERR_SYS_VEND_CONF_OPEN_FAIL      18299

#define ERR_SYS_VEND_CONF_CHECK_FAIL     18300
#define ERR_SYS_VEND_CONF_ENCODE_FAIL    18301
#define ERR_SYS_VEND_CONF_DECODE_FAIL    18302
#define ERR_CHECK_FIRMWARE               18303

#define ERR_NO_PACKAGE                   18304
#define ERR_AVERAGE_TRIM                 18305
#define ERR_ADDR_SET_FAIL                18306
#define ERR_PWD_LIMIT_6                  18307

#define ERR_TWO_DIFFERENT                18308
#define ERR_INVALID_OLD                  18309
#define ERR_INVALID_PASS                 18310
#define INF_50_OVERLOAD                  18311

#define INF_HDMI_INSERT                  18312
#define INF_HDMI_UNINSERT                18313
#define INF_DG_PROTECTED_G1              18314
#define INF_DG_PROTECTED_G2              18315

#define INF_UNKNOWN_USB                  18316
#define INF_PROBE_DISCONNECT             18317
#define INF_PROBE_CONNECTED              18318
#define INF_USB_STORAGE_CONNECT          18319

#define INF_USB_STORAGE_DISCONNECT       18320
#define INF_TOUCH_ENABLE                 18321
#define INF_TOUCH_DISABLE                18322
#define INF_LAN_CONNECTED                18323

#define INF_WLAN_CONNECTED               18324
#define INF_LAN_DISCONNECTED             18325
#define INF_WLAN_DISCONNECTED            18326
#define INF_USBTMC_CONNECTED             18327

#define INF_USBTMC_DISCONNECTED          18328
#define INF_LXI_CONNECTED                18329
#define INF_LXI_DISCONNECTED             18330
#define INF_LXI_CEILING                  18331

#define INF_USB_GPIB_CONNECTED           18332
#define INF_USB_GPIB_DISCONNECTED        18333
#define INF_USB_GPIB_NOT_CONNECTED       18334
#define INF_SOCKET_CONNECTED             18335

#define INF_SOCKET_DISCONNECTED          18336
#define INF_SOCKET_CEILING               18337
#define INF_DEFAULT                      18338
#define INF_BEFORE_UPDATE                18339

#define INF_SCR_MSG                      18340
#define INF_MAIL_SMTP                    18341
#define INF_MAIL_PORT                    18342
#define INF_MAIL_USER                    18343

#define INF_MAIL_PASS                    18344
#define INF_MAIL_SETTING                 18345
#define INF_MAIL_TEST_OK                 18346
#define INF_MAIL_TEST_FAIL               18347

#define INF_SYS_INFO                     18348
#define INF_SYS_VENDOR                   18349
#define INF_SYS_MODEL                    18350
#define INF_SYS_SERIAL                   18351

#define INF_SYS_FIRMWARE                 18352
#define INF_SYS_HARDWARE                 18353
#define INF_SYS_BOOT                     18354
#define INF_SYS_BUILD                    18355

#define INF_SECURITY_CLEAR               18356
#define INF_UPGRADE_MSG                  18357
#define INF_UPGRADE_H1                   18358
#define INF_UPGRADE_H2                   18359

#define INF_UPGRADE_H3                   18360
#define INF_UPGRADE_H4                   18361
#define INF_MODULE_DEFAULTING            18362
#define INF_MODULE_DEFAULT               18363

#define MSG_OPT_FILE_DETECT              18364
#define MSG_OPT_ALREADY_ACT              18365
#define MSG_OPT_BW_OPT_INVALID           18366
#define MSG_OPT_TRIAL_LIMIT              18367

#define MSG_OPT_FAILED_LIMIT             18368
#define MSG_OPT_RETRY                    18369
#define MSG_OPT_LIC_UNUSABLE             18370
#define MSG_OPT_KEY_INVALID              18371

#define MSG_OPT_SETUP_SUCCESS            18372
#define ERR_CAL_LA_THRESHOLD             18373
#define ERR_AUTO_NO_SIGNAL               18374
#define ERR_AUTO_OVERRANGE_SIGNAL        18375

#define ERR_AUTO_DISABLED                18376
#define ERR_AUTO_UNLOCKED                18377
#define ERR_AUTO_LOCKED                  18378
#define ERR_CAL_LOAD_FAIL                18379

#define ERR_CAL_LOAD_SUCCESS             18380
#define ERR_CAL_EXPORT_FAIL              18381
#define ERR_CAL_EXPORT_SUCCESS           18382
#define ERR_CAL_PHASE                    18383

#define ERR_MASK_NO_TRACE                18384
#define ERR_PROBE_CAL_SUCCEED            18385
#define ERR_PROBE_CAL_FAILURE            18386
#define ERR_PROBE_REQUEST_CAL            18387

#define INF_APP_HORIZONTAL               18388
#define INF_APP_VERTICAL                 18389
#define INF_APP_OTHER                    18390
#define INF_APP_ANALYZE                  18391

#define INF_ITEM_PERIOD                  18392
#define INF_ITEM_FREQ                    18393
#define INF_ITEM_RISETIME                18394
#define INF_ITEM_FALLTIME                18395

#define INF_ITEM_PWIDTH                  18396
#define INF_ITEM_NWIDTH                  18397
#define INF_ITEM_PDUTY                   18398
#define INF_ITEM_NDUTY                   18399

#define INF_ITEM_PPULSES                 18400
#define INF_ITEM_NPULSES                 18401
#define INF_ITEM_PEDGES                  18402
#define INF_ITEM_NEDGES                  18403

#define INF_ITEM_TVMAX                   18404
#define INF_ITEM_TVMIN                   18405
#define INF_ITEM_PSLEWRATE               18406
#define INF_ITEM_NSLEWRATE               18407

#define INF_ITEM_DELAY                   18408
#define INF_ITEM_PHASE                   18409
#define INF_ITEM_VMAX                    18410
#define INF_ITEM_VMIN                    18411

#define INF_ITEM_VPP                     18412
#define INF_ITEM_VTOP                    18413
#define INF_ITEM_VBASE                   18414
#define INF_ITEM_VAMP                    18415

#define INF_ITEM_VUPPER                  18416
#define INF_ITEM_VMID                    18417
#define INF_ITEM_VLOWER                  18418
#define INF_ITEM_VAVG                    18419

#define INF_ITEM_VRMS                    18420
#define INF_ITEM_PER_VRMS                18421
#define INF_ITEM_OVERSHOOT               18422
#define INF_ITEM_PRESHOOT                18423

#define INF_ITEM_AREA                    18424
#define INF_ITEM_PER_AREA                18425
#define INF_ITEM_VARIANCE                18426
#define INF_LAN_SETTING                  18427

#define INF_LAN_STATUS                   18428
#define INF_IP_TYPE                      18429
#define INF_MAC_ADDR                     18430
#define INF_VISA_ADDR                    18431

#define INF_IP_ADDR                      18432
#define INF_SUBNET                       18433
#define INF_GATEWAY                      18434
#define INF_DNS                          18435

#define INF_DHCP                         18436
#define INF_AUTOIP                       18437
#define INF_STATIC_IP                    18438
#define INF_CAL_NOTICE                   18439

#define INF_CAL_INFO                     18440
#define INF_CAL_LAST_TIME                18441
#define INF_CAL_LAST_RESULT              18442
#define INF_CAL_CURR_RESULT              18443

#define INF_UPGRADE_TITLE                18444
#define INF_UPGRADE_AGREE                18445
#define INF_UPGRADE_CANCEL               18446
#define INF_UPGRADE_CONTEXT              18447

#define INF_INDICATOR_DISABLED           18448
#define INF_NAVIGATION_HOR_STOP          18449
#define INF_SYSTEM_BW                    18450
#define INF_CONFIG_WAITING               18451

#define INF_FEATURE_NOT                  18452
#define INF_LA_CALIBRATING               18453
#define INF_ZONE_SMALL                   18454
#define INF_TOUCH_ZONE                   18455

#define INF_TOUCH_CONTROL                18456
#define INF_DISABLE_EYEJIT               18457
#define INF_JIT_ANALYZING                18458
#define INF_EYE_ANALYZING                18459

#define INF_DISABLE_JIT                  18460
#define INF_DISABLE_EYE                  18461
#define INF_HRES_BW                      18462
