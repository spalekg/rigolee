#!/bin/sh -eu
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright 2019 (C) Olliver Schinagl <oliver@schinagl.nl>
#

set -eu

USB_STORAGE="${USB_STORAGE:-/media/sda1/}"
PUBLIC_KEYS="${PUBLIC_KEYS:-${USB_STORAGE}/authorized_keys}"

if [ -f "${PUBLIC_KEYS}" ]; then
    mkdir -p "/root/.ssh/"
    cat "${PUBLIC_KEYS}" >> "/root/.ssh/authorized_keys"
fi

/etc/init.d/S50sshd restart

exit 0
